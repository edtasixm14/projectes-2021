# Projectes 2021

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2020-2021

#### M14 Projecte

1) Hicham elMeriouli Varo: **Snort**

https://gitlab.com/isx39444871/Projecte_ASIX.git 

2) Andreu Passalamar Carbó i Carles grilló Oller: **NetxCloud**

https://github.com/isx20612296/projecte-nextcloud 

3) Alejandro López Herrador i Roberto Martínez Conejero: **Kubernetes**

https://github.com/zeuslawl/Kubernetes 

4) Christian Manalo Mañibo i Mark Santiago Burgos: **Jenkins**

https://github.com/isx47328890/projecte-jenkins 

5) Javier Moyano Vejarano: **Pacemaker**

https://github.com/JaviMoyano/Projecte-Pacemaker 

6) Diego  Sánchez Piedra: **Vagrant / Terraform / Packer**

https://github.com/isx2031424/projecte-edt 

7) Mati Vizcaino Sánchez: **OpenStack**

https://github.com/mattvizcaino/openstack 

8) Adrian Narváez: **Kubernetes**

https://gitlab.com/edtasixm14/2021-kubernetes-adri

 
